Title: 
Date: 2022-03-14
save_as: index.html
status: hidden


Lähitulevaisuuden pahasti jakautuneessa Suomessa suurten yritysten työntekijöitä irtisanoutuu selittämättömästi. Energiayhtiön pääomistajalta varastetaan huomattava summa rahaa. Johtolangat alkavat osoittamaan kohti Kainuun vapaavaltiota, joka on alun perin perustettu ottamaan vastaan Venäjän hybridioperaatioiden tuottama pakolaisvirta. 

T

> On kaksi romaania jotka voivat muuttaa 14-vuotiaan kirjoihin menevän nuoren elämän: Sormusten herra ja "Atlas Shrugged". Toinen on lapsellinen fantasia joka saa usein aikaan koko elämän mittaisen pakkomielteen sen uskomattomiin sankareihin, mikä johtaa sosiaalisesti ja tunteellisesti vajaaseen aikuiselämään ja kykenemättömyyteen kohdata tosielämä. Toisessa, tietysti, on örkkejä.
> 
> John Rogers






Title: Kappale 1
Date: 2022-03-14
Category: Kappaleet


“Irtisanoutumiseni”. Tuo tietokoneen näytöllä loistava sana sai Tainan
lysähtämään takaisin penkille ja kiroamaan ääneen. Hän oli juuri ollut
sulkemassa konetta, kun uusi viesti oli tullut näytölle. Hän avasi
sähköpostin ja luki sen nopeasti läpi.

Taina kirosi uudestaan. Eddie, naapuripöydässä istuva skotlantilainen
ydinfyysikko, vilkaisi häntä, mutta jatkoi keskittyneesti yhtälöiden
kirjoittamista ruutupaperille vanhanaikaisella teroitettavalla
lyijykynällä. Taina nojasi tuolissaan taaksepäin ja huokaisi. Ville oli
kolmas keskeinen Thorium-projektin henkilö, joka oli irtisanoutunut
viimeisen kahden viikon aikana. Kaikki kolme näyttivät irtisanoutuneen
samalla tyylillä. He olivat lähettäneet kohteliaan ja pahoittelevan
sähköpostin henkilökohtaisesti Tainalle, ja byrokratia oli hoidettu
HR-osaston kanssa niin, että irtisanomisaikana käytetään käyttämättömät
lomat. Käytännössä he olivat vain hävinneet. Eveliina ja Antti olivat
lähteneet samana päivänä. Silloin Taina ei ollut ärtymykseltään edes
soittanut perään, vaan oli ajatellut että menkööt, jos eivät viitsineet
tulla hyvästelemään kasvotusten.

Taina luki viestin vielä kerran. Loppuun päästyään hän hätkähti ja
kumartui lähemmäs ruutua. Viesti ei loppunutkaan Villen nimeen, vaan sen
jälkeen oli muutama rivi tyhjää ja sitten kuin ohimennen kirjoitettuna
hashtag:

\#missäjussion

Taina kaivoi esiin Eveliinan ja Antin irtisanoutumisviestit ja löysi
saman hashtagin molempien viestien lopusta. Taina nojasi taaksepäin
tuolillaan ja hieroi ohimoitaan. Hänen mieleensä tulvahtivat
opiskeluajan muistot siitä, kun hashtag oli syntynyt. Jussi oli päässyt
kesätöihin nimekkääseen konsultointi- ja tilintarkastusyritykseen.
Jossain kohtaa ensimmäistä projektia Jussi oli ymmärtänyt, että
projektissa oli pohjimmiltaan kyse kansainvälisestä verojen välttelystä.
Seuraavana päivänä Jussi ei ollut enää mennyt töihin, vaan julkaisi
tietonsa sosiaalisessa mediassa tuolla hashtagilla varustettuna. Se oli
jäänyt elämään tunnisteena, jolla ihmiset kieltäytyivät tekemästä
epäeettistä työtä.

Ja se oli jotakin, missä ei Tainan mielestä ollut mitään järkeä. Heidän
projekteissaan ei ollut mitään epäeettistä. Kaikki kolme olivat olleet
aivan täysillä projektissa mukana.

Tainan ajatus harhautui taas Jussiin, ja hän pohdiskeli mitä Jussille
kuului. Edellisestä tapaamisesta oli vuosia.

Sitten hän palasi miettimään irtisanoutuneita, ja päätti puhua heidän
kanssaan selvittääkseen, mitä tässä oikein oli takana.

Hän kaivoi puhelimen taskustaan ja soitti Villelle. Pienen odotuksen
jälkeen puhelimesta kuului ääni:

“Valitsemaanne numeroon ei juuri nyt saada yhteyttä.”

Taina kurtisti kulmiaan ja kaivoi esiin Eveliinan numeron.

“Valitsemaanne numeroon ei juuri nyt saada yhteyttä.”

Hän epäili jo tietävänsä Antin puhelimen vastauksen, mutta kokeili
silti:

“Valitsemaanne numeroon ei juuri nyt saada yhteyttä.”

Hän mietti, kuka HR-osastolta suostuisi kaivamaan lähiomaisten tietoja
esiin, kun Jaakko käveli avokonttoriin ja katsoi merkitsevästi Tainan
farkkuja ja flanellipaitaa.

“Mitä sinä vielä täällä teet? Et kai ajatellut jättää
hyväntekeväisyysgaalaa väliin?”

Taina katsoi veljeään, joka hymyili rennosti mittatilaustyönä tehdyssä
puvussaan. Jälleen kerran, kuten viime aikoina lähes aina veljensä
tavatessaan, hän epäili, että vanhemmat eivät olleet kertoneet kaikkea
lastensa keskinäisistä sukulaisuussuhteista.

“Olin jo lähdössä, mutta yksi meidän ryhmästä irtisanoutui taas. Kolmas
kahteen viikkoon.”

Jaakko vilkaisi kelloaan ja vastasi:

“Selvitä uusia rekrytointeja huomenna HR:n kanssa. Kello on jo aika
paljon. Älä myöhästy.”

“Huomenna? Et ole tainnut olla kovin paljon tekemisissä HR-osaston
kanssa? Olen jo toista viikkoa yrittänyt saada rekrytointiprosessin
liikkeelle kahden edellisen irtisanoutuneen tilalle, mutta siellä on
kiireitä kesäjuhlien järjestämisen kanssa, tai jotain. Etkä taida
ymmärtää, kuinka harvinaista osaamista tarvitsemme. Tämä on ihan oikea
ongelma. Sitä paitsi noissa irtisanoutumisissa on jotain ihmeellistä.
Kenenkään numeroon ei saa yhteyttä. Ihan kuin ne olisivat hävinneet maan
päältä.”

Ennen viimeisen lauseen loppua Jaakko oli jo ehtinyt jatkaa matkaa.

Taina lukitsi tietokoneensa, nousi ja sanoi Eddielle:

“Hei Eddie, joudun nyt lähtemään sinne typerään hyväntekeväisyysgaalaan.
Ville irtisanoutui juuri. En usko, että hän tulee sanomaan hyvästejä,
joten varaa kalenteristani aika huomiselle, niin mietitään miten
edetään.”

“Ahaa, luulin että kirosit iltaohjelmaasi. Mutta Ville? Miksi?”

“Henkilökohtaiset syyt, ei eritellyt sen tarkemmin. Mutta viestissä oli
hashtag \#missäjussion. Puhelimeen ei saa yhteyttä. Jos sinulla on
aikaa, niin koita saada Ville, Antti tai Eveliina jollakin tavalla
kiinni, haluaisin jutella niiden kanssa. Mutta puhutaan huomenna
enemmän, olen jo nyt vähän myöhässä.”

“Eihän tuossa ole tosiaan mitään järkeä”, Eddie sanoi. Hänen silmänsä
kirkastuivat, kun hän jatkoi:

“Mitenköhän ne löytäisi? Ainakin sosiaalisen median tilejä pitäisi
tutkia, jos sieltä löytyy vihjeitä. Tai sukulaisia ja kavereita. Ehkä
joku on jättänyt Strava-tilinsä auki, sieltä näkisi pyörälenkit.”

Taina nauroi ja keskeytti Eddien pohdiskelutulvan.

“Kiitos, tiesin, että sinuun voi luottaa.”

Taina lähti autohallia kohti. Muu Fartumin konttori oli jo hiljainen.
Fartum koostui kahdesta liiketoimintayksiköstä,
sähköverkkoliiketoiminnasta ja ydinjätteen
loppusijoitusliiketoiminnasta. Sähköverkkojen parissa työskentelevät
tekivät töitä kuten monopoliyhtiössä yleensä, eli harvoin kukaan
yliaikaa istui. Loppusijoituspuolella taas oli start-up henkinen
työilmapiiri, ja ongelmia pohdittiin usein iltamyöhään. Asiasta oli
joskus puhuttu kehityskeskusteluissa, ja syy ilmapiirille oli selvä.
Työntekijät kokivat tekevänsä työtä, jolla on merkitys, ja ryhmään oli
kerääntynyt alansa terävin kärki, joka arvosti suuresti toistensa
osaamista.

Se, miksi nämä liiketoimintayksiköt olivat saman katon alla, oli
Tainalle jokseenkin yhtä suuri mysteeri kuin se, miten Jaakko oli
päätynyt Fartumin yksityistämisessä sen merkittäväksi omistajaksi.

Taina otti autohallista polkupyöränsä ja talutti sen ulos. Hän kiitteli
onneaan, että viime päivien poikkeuksellisen kovat koillistuulet olivat
helpottaneet.Ilma oli lähes tyyni, kun hän lähti polkemaan Keilaniemestä
Leppävaaraan, kotia kohti.

Vähän ennen Turunväylää hän pysähtyi tarkastuspisteeseen näyttämään
henkilöpapereitaan. Tähän suuntaan tarkastus meni nopeasti; aamuisin oli
yleensä hitaampaa. Tarkastuspisteen jälkeen oli helppo huomata, että
siirryttiin Länsi-Helsingin kaupungista Espooseen. Pyörätiet olivat
huonossa kunnossa, roskia oli vähän joka puolella, eikä viheralueita
ollut kunnostettu. Heti tarkastuspisteen takana oli - kuten melkein aina
- Tainalle kasvoista tutuksi tullut vaihteleva joukko laitapuolen
kulkijoita. Tällä kertaa paikalla oli kolme miestä: vanhempi, varmasti
eläkeikää lähentelevä, vanhaan tuulipukuun pukeutunut partainen mies,
ehkä kolmekymppinen, kapeakasvoinen ja hintelä mies, joka oli vetänyt
hupparin hupun syvälle päähänsä, ja noin nelikymppinen, roteva
siilitukkainen mies. Ja kuten monena iltana aikaisemminkin, porukan
vanhin mies tuli tervehtimään Tainaa.

“Hyvää iltaa nuori neiti. Vähän aikaisemmin liikkeellä kuin
normaalisti?”

“No ei tässä nyt enää niin nuoria olla. Vähän työohjelmaa illalla.”

Miehen hengityksessä haisi vanha viina.

“Niin se taitaa nykypäivänä olla, että ne kenellä työtä on, on sitä
liikaa, ja sitten on meitä jotka eivät enää kenellekään kelpaa. Mikä
sinua vielä aidan tällä puolella pitää? Miksi et muuta tuonne parempien
ihmisten puolelle, kun siellä töissäkin olet?”

Taina kohautti olkapäitään.

“Asuin täällä, kun Länsi-Helsingistä tehtiin oma aidattu kaupunkinsa
paremmalle väelle, ja viihdyn täällä ihan hyvin. Pintakiilto aidan
tuolla puolen menee pidemmän päälle ihon alle.”

Mies röhähti.

“Minua ei pieni pintakiilto välillä haittaisi. Mutta eipä sinne
tällaisia ihmisiä päästetä.”

Kapeakasvoinen mies avasi suunsa.

“Kuulemma vapaavaltioon Kainuuseen voisi mennä. Saisi asunnon ja
ruokaa.”

Siilitukkainen mies katsahti äkäisesti kapeakasvoiseen mieheen ja
tokaisi:

“Sinne neekerien ja muslimien sekaan en ainakaan lähde. Kun ne kerran
saatiin aidattua sinne suomalaista kulttuuria pilaamasta, niin pysykööt
siellä.”

Taina hyppäsi pyörän päälle ja sanoi:

“Anteeksi, joudun nyt jatkamaan matkaa.” Tainan lähtiessä liikkeelle
vanhempi mies lähti perään ja kysyi:

“Olisiko heittää vanhalle miehelle kolikkoa iltapalaa varten ?”

Vielä joitakin vuosia sitten Taina olisi vain todennut, että Suomen
sosiaaliturvalla kenenkään ei tarvitse kerjätä, mutta valtion
säästökuurien jäljiltä sosiaaliturva oli siirretty kokonaisuudessaan
kuntien harteille. Sen seurauksena köyhempien alueiden sosiaaliturva oli
rapautunut niin paljon, että toimeentuloa ei voinut enää laskea sen
varaan. Siispä Taina oli alkanut silloin tällöin antaa rahaa sitä
tarvitseville.

“Iltapalaan vai loiventavaan?” hän ei kuitenkaan malttanut olla
kysymättä.

Mies laski ensin katseensa vähän häveten maahan, vilkaisi sitten, missä
muut olivat ja sanoi hiljaa Tainalle:

“Säästän junalippuun, vapaavaltioon. Ei tällainen vanha mies enää halua
ensi talvea kadulla viettää.”

Taina mietti hetken.

“Kuule. Jos se on sinun suunnitelmasi, niin minä ostan sinulle lipun. Ei
tarvitse pelätä säästöjen hukkumista matkan varrella.”

Mies katsoi Tainaa.

“Mutta... En minä vielä halua lähteä. Syksyllä vasta. Tämä on kuitenkin
kotini.”

Taina hymyili.

“No, tästä sinä minut usein löydät työpäivän jälkeen. Sen kun sanot, kun
haluat lähteä.”

Mies katsoi Tainaa hämmentyneen oloisena.

“Miksi olet noin avulias? Ei sinun tarvitsisi. Suurin osa vain nyrpistää
nenäänsä ja menee ohi.”

Taina nauroi.

“Miksikö? Itsekkäistä syistä. En minä niin tyhmä ole, etten huomaisi
että yhä useammalla ihmisellä menee huonosti. Vaikka minua ei pelota
vielä, niin jossain vaiheessa joku saa varmasti päähänsä, että tuolta
parempien ihmisten alueelta tulevilta saa väkivalloin rahaa irti. Siinä
kohtaa minulla varmaan kannattaa olla kavereita sellaisissa piireissä,
joissa tiedetään mitä alueella tapahtuu. Vai mitä olet mieltä?”

Taina iski silmää ja kaivoi taskustaan pari kolikkoa miehelle.

“Osta ennemmin leipää kuin muita mallastuotteita iltapalaksi.”

Mies jäi katsomaan hiljaisena, kun Taina lähti polkemaan kohti
Leppävaaraa. Tainan silmään osui moottoritien meluvallissa uusi
graffiti. Jostain syystä häntä alkoi ärsyttää suunnattomasti, kun hän
tajusi mitä siinä luki:

\#missäjussion
